using Lab3.Library;

namespace Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ValueIsEqual()
        {
            var expected = 10.045;
            var actual = new Pyramid(10, 3, 2).SideSurfaceArea;
            Assert.AreEqual(expected, actual, 0.03);
        }

        [Test]
        public void HeightLessThan0Exception()
        {
            Assert.Throws<Exception>(() => new Pyramid(-1, 4, 2));
        }

        [Test]
        public void NumberOfSidesLessThan3Exception()
        {
            Assert.Throws<Exception>(() => new Pyramid(5, 1, 15));
        }
    }
}