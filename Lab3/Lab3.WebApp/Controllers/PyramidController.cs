﻿using Lab3.WebApp.Models;
using Lab3.WebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab3.WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PyramidController : ControllerBase
    {
        private PyramidSideSurfaceAreaService _pyramidSideSurfaceArea => HttpContext.RequestServices.GetService<PyramidSideSurfaceAreaService>();
        protected JsonResult JSON<TValue>(TValue obj) => new(obj);

        [HttpPost]
        public async Task<JsonResult> GetPyramidSideSurfaceArea([FromBody] PyramidEntity entity) => JSON(await _pyramidSideSurfaceArea.CountPyramidSideSurfaceArea(entity));
    }
}
