﻿using System.ComponentModel.DataAnnotations;

namespace Lab3.WebApp.Models
{
    public class PyramidEntity
    {
        [Required]
        public double Height { get; set; }
        [Required]
        public uint SideCount { get; set; }
        [Required]
        public double SideLength { get; set; }
    }
}
