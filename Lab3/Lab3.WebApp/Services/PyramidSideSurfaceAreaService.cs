﻿using Lab3.Library;
using Lab3.WebApp.Models;

namespace Lab3.WebApp.Services
{
    public class PyramidSideSurfaceAreaService
    {
        public async Task<double> CountPyramidSideSurfaceArea(PyramidEntity entity)
        {
            return new Pyramid(entity.Height, entity.SideCount, entity.SideLength).SideSurfaceArea;
        }
    }
}
