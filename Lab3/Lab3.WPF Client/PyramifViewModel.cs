﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Lab3.Library;

namespace Lab3.WPF_Client;

internal class PyramifViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    public void OnPropertyChanged([CallerMemberName]string prop = "")
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
    private double _pyramidSideSurfaceArea;


    public double PyramidSideSurfaceArea { get => _pyramidSideSurfaceArea; set  { _pyramidSideSurfaceArea = value; OnPropertyChanged(); } }
    public double PyramidHeight { get => _pyramidHeihgt; set { _pyramidHeihgt = value;  OnPropertyChanged(); PyramidSideSurfaceArea = new Pyramid(_pyramidHeihgt, _pyramidSideAmount, _pyramidSideLength).SideSurfaceArea; } }
    private double _pyramidHeihgt;

    public uint PyramidSideAmount { get => _pyramidSideAmount; set { _pyramidSideAmount = value; OnPropertyChanged(); PyramidSideSurfaceArea = new Pyramid(_pyramidHeihgt, _pyramidSideAmount, _pyramidSideLength).SideSurfaceArea; } }
    private uint _pyramidSideAmount;

    public double PyramidSideLength { get => _pyramidSideLength; set { _pyramidSideLength = value; OnPropertyChanged(); PyramidSideSurfaceArea = new Pyramid(_pyramidHeihgt, _pyramidSideAmount, _pyramidSideLength).SideSurfaceArea; } }
    private double _pyramidSideLength;
    public PyramifViewModel()
    {

    }
}
