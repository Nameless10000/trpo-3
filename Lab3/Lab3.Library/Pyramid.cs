﻿namespace Lab3.Library
{
    public class Pyramid
    {
        public Pyramid(double height, uint numberOfSides, double sideLength)
        {
            if (height <= 0)
                throw new Exception("Heiht must be more than 0");

            if (numberOfSides < 3)
                throw new Exception("number of sides must be more or equal 3");

            Height = height;
            NumberOfSides = numberOfSides;
            SideLength = sideLength;
        }

        public double Height { get; set; }
        public uint NumberOfSides { get; set; }
        public double SideLength { get; set; }

        public double SideSurfaceArea => Math.Sqrt(Math.Pow(Height, 2) + Math.Pow(SideLength / (2 * Math.Sin(Math.PI / NumberOfSides)), 2));
    }
}