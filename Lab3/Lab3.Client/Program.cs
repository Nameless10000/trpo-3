﻿using Lab3.Library;

namespace Lab3.Client
{
    class Programm
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите высоту пирамиды: ");
            var height = (Console.ReadLine() ?? "").TryParseToDouble();

            Console.WriteLine("Введите кол-во граней пирамиды: ");
            var numberOfSides = (Console.ReadLine() ?? "").TryParseToUint();

            Console.WriteLine("Введите длину основания грани: ");
            var sideLength = (Console.ReadLine() ?? "").TryParseToDouble();

            if (sideLength != null && numberOfSides != null && height != null)
            {
                try
                {
                    var pyramid = new Pyramid((double)height, (uint)numberOfSides, (double)sideLength);
                    Console.WriteLine($"Площадь боковой поверхности пирамиды = {pyramid.SideSurfaceArea}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return;
            }
            Console.WriteLine("Некоректные входные данные");
        }
    }
}