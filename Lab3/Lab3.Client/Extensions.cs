﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Client
{
    public static class Extensions
    {
        public static double? TryParseToDouble(this string str)
        {
            if (double.TryParse(str.Replace(".", ","), out double res))
                return res;
            else return null;
        }

        public static uint? TryParseToUint(this string str)
        {
            if (uint.TryParse(str.Replace(".", ","), out uint res))
                return res;
            else return null;
        }
    }
}
