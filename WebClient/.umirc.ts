import { CalculatorOutlined } from '@ant-design/icons';
import { defineConfig } from '@umijs/max';

export default defineConfig({
  antd: {},
  access: {},
  model: {},
  locale: { default: 'ru-RU', antd: true },
  initialState: {},
  request: {},
  layout: {
    title: 'Pyramid claculator',
  },
  routes: [
    {
      path: '/',
      redirect: '/calculator',
    },
    {
      name: 'Calculator',
      path: '/calculator',
      component: '@/pages/Calculator',
    }
  ],
  npmClient: 'npm',
});

