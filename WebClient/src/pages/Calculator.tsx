import { PageContainer, ProField, ProForm, ProFormContext, ProFormDigit, ProFormItem } from "@ant-design/pro-components"
import { request } from "@umijs/max";
import { Popover } from "antd"
import { useState } from "react";
import { PyramidDTO } from "typings"

export default () => {

    const [result, setResult] = useState<number>(0); 
    const [resOpen, setResOpen] = useState<boolean>(false);

    const formFinishHandler = async (data: PyramidDTO) => {
        request("https://localhost:7204/api/Pyramid/GetPyramidSideSurfaceArea", {
            method: "POST",
            data
        })
        .then((result: number) => {
            setResult(result);
            setResOpen(true);
        })
    }

    return <>
        <PageContainer>
            <ProForm onFinish={formFinishHandler}
            submitter={{
                searchConfig: {
                    resetText: "Reset values",
                    submitText: "Count"
                },
                render(_, dom) {
                    return dom.reverse();
                },
            }}
            onReset={() => {setResult(0); setResOpen(false)}}>
                <ProFormDigit name="height" label="Pyramid height" required rules={[{required: true, message: "Enter the pyramid height"}]}/>
                <ProFormDigit name="sideCount" label="Count of sides" required rules={[{required: true, message: "Enter the count of sides"}]}/>
                <ProFormDigit name="sideLength" label="Side length" required rules={[{required: true, message: "Enter the side length"}]}/>
            </ProForm>
            <br/>
            <br/>
            <br/>
            {(resOpen && <ProField mode={"read"} text={`Pyramid side surface area is: ${result.toFixed(3)}`}/>)}
        </PageContainer>
    </>
}